<?php get_header();
$theme       = get_bloginfo( 'template_directory' );
$name        = get_bloginfo( 'name' );
$description = get_bloginfo( 'description' ) ?>
<section id="mainContent">
	<?php
	if ( function_exists( 'yoast_breadcrumb' ) ) {
		yoast_breadcrumb( '<div id="breadcrumbs">', '</div>' );
	} ?>
	<?php
	$category_id = get_cat_ID(single_cat_title( '', false));
	$category_link = get_category_link( $category_id );
	?>
    <h2><?php single_cat_title() ?></h2>
	<?php the_archive_description( '<div class="taxonomy-description">', '</div>' );
	if ( have_posts() ) : ?>
        <div class="row">
			<?php while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content', get_post_format() );
			endwhile; ?>
        </div>
		<?php
		echo bootstrap_pagination();
	else :
		get_template_part( 'template-parts/content', 'none' );
	endif; ?>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
