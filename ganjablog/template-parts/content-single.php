<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        <div class="entry-header-meta">
            <time class="entry-date" datetime="<?php echo get_the_date( 'Y-m-d H:i' ) ?>">
                <span class="fas fa-calendar-alt"></span> <?php echo get_the_date( 'd.m.Y', '', '' ); ?>,
            </time>
            <span class="entry-categories"><span class="fas fa-folder"></span> <?php the_category( ', ' ); ?></span>
            <span class="fas fa-comment-alt"></span>
            <a href="<?php echo get_comments_link() ?>"><?php echo get_comments_number() ?></a>
            <span class="fas fa-eye"></span>
			<?php echo_post_views( get_the_ID() ); ?>
        </div>
    </header>
    <div class="clearfix">
        <div class="entry-thumbnail float-lg-left">
			<?php the_post_thumbnail( 'large', array( 'class' => 'img-fluid' ) ); ?>
        </div>
        <div class="entry-content">
			<?php
			the_content();
			wp_link_pages(
				array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'ganjablog' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'ganjablog' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				)
			);
			?>
        </div>
    </div>
    <footer class="entry-footer">
        <h3>Поделиться с друзьями</h3>
        <div class="social-share">
            <div class="container-fluid">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <a href="https://vk.com/share.php?url=<?php the_permalink(); ?>" class="social-vk"
                           target="_blank">
                            <i class="fab fa-vk" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&amp;t=<?php the_title(); ?>"
                           class="social-fb" target="_blank">
                            <i class="fab fa-facebook" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=<?php the_permalink(); ?>&st.comments=<?php the_title(); ?>"
                           class="social-ok" target="_blank">
                            <i class="fab fa-odnoklassniki" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php the_title(); ?>&amp;hashtags=my_hashtag"
                           class="social-tw" target="_blank">
                            <i class="fab fa-twitter" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"
                           class="social-pt">
                            <i class="fab fa-pinterest" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink() ?>&title=<?php the_title(); ?>&summary=&source=<?php bloginfo( 'name' ); ?>"
                           target="_new">
                            <i class="fab fa-linkedin" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="http://www.reddit.com/submit"
                           onclick="window.location = 'http://www.reddit.com/submit?url=' + encodeURIComponent(window.location); return false">
                            <i class="fab fa-reddit" aria-hidden="true"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
</article>
