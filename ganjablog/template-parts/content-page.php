<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
    </header>
    <div class="entry-content">
		<?php if ( has_post_thumbnail() ) { ?>
            <div class="entry-thumbnail text-center">
				<?php the_post_thumbnail( 'large', array( 'class' => 'img-fluid' ) ); ?>
            </div>
		<?php } ?>
		<?php
		the_content();
		wp_link_pages(
			array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'ganjablog' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="sr-only">' . __( 'Page', 'ganjablog' ) . ' </span>%',
				'separator'   => '<span class="sr-only">, </span>',
			)
		);
		?>
    </div>
</article>