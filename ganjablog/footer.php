</div>
</main>
<?php
$theme       = get_bloginfo( 'template_directory' );
$name        = get_bloginfo( 'name' );
$description = get_bloginfo( 'description' ); ?>
<div id="imageInHeader">
	<?php
	if ( get_theme_mod( 'image_control_one' ) ) { ?>
        <img src="<?php echo get_theme_mod( 'image_control_one' ); ?>"
             class="img-fluid"
             alt="<?php echo $description ?>"/>
	<?php }
	?>
</div>
<footer id="mainFooter">
    <div class="container">
        <div class="row">
            <div class="copyright col-lg-6">
                <p>&copy; <?php echo date( 'Y' ) ?>, <?php echo $name ?></p>
                <p><?php echo $description ?></p>
            </div>
            <div class="footer-menu col-lg-6">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'footer',
					'depth'          => 1,
					'container'      => false,
					'menu_class'     => '',
					'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
					// Process nav menu using our custom nav walker.
					'walker'         => new WP_Bootstrap_Navwalker(),
				) );
				?>
            </div>
        </div>
    </div>
</footer>
</div>
<script src="<?php echo $theme ?>/js/app.js" async defer></script>
<?php wp_footer(); ?>
</body>
</html>