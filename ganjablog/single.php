<?php get_header(); ?>
<section id="mainContent">
	<?php
	if ( function_exists( 'yoast_breadcrumb' ) ) {
		yoast_breadcrumb( '<div id="breadcrumbs">', '</div>' );
	} ?>
	<?php while ( have_posts() ) : the_post();
		get_template_part( 'template-parts/content', 'single' );

		if ( is_singular( 'attachment' ) ) {
			the_post_navigation(
				array(
					'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'ganjablog' ),
				)
			);
		} elseif ( is_singular( 'post' ) ) {
			$prev_post   = get_previous_post();
			$id          = $prev_post->ID;
			$permalink_p = get_permalink( $id );

			$next_post   = get_next_post();
			$nid         = $next_post->ID;
			$permalink_n = get_permalink( $nid ); ?>
            <div class="fixed-nav-buttons">
				<?php if ( $next_post ) { ?>
                    <div class="next-post">
                        <a href="<?php echo $permalink_n; ?>" data-toggle="tooltip"
                           title="<?php echo $next_post->post_title; ?>">
                            <span class="fas fa-angle-left"></span>
                        </a>
                    </div>
				<?php } ?>
				<?php if ( $prev_post ) { ?>
                    <div class="prev-post">
                        <a href="<?php echo $permalink_p; ?>" data-toggle="tooltip"
                           title="<?php echo $prev_post->post_title; ?>">
                            <span class="fas fa-angle-right"></span>
                        </a>
                    </div>
				<?php } ?>
            </div>
            <h2 class="page-block-header"><?php _e( 'Related posts', 'ganjablog' ) ?></h2>
            <div class="related-posts container-fluid">
                <div class="row">
					<?php global $post;
					$post_cat = get_the_category( $post->ID );
					get_random_posts_from_category( $post_cat[0]->cat_ID ) ?>
                </div>
            </div>

		<?php }
		if ( comments_open() || get_comments_number() ) { ?>
            <h2 class="page-block-header"><?php _e( 'Related posts', 'ganjablog' ) ?></h2>
			<?php
			comments_template();
		}
	endwhile; ?>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
