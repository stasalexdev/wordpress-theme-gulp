<?php get_header(); ?>
<section id="mainContent">
    <h2 class="page-title"><?php _e( 'That page can&rsquo;t be found.', 'ganjablog' ); ?></h2>
    <div class="page-content">
        <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'ganjablog' ); ?></p>
        <div class="row">
            <div class="col-lg-6">
				<?php get_search_form(); ?>
            </div>
        </div>
    </div>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
