<?php
// Setup the blog
if ( ! function_exists( 'ganjablog_setup' ) ) :

	function ganjablog_setup() {

		load_theme_textdomain( 'ganjablog', get_template_directory() . '/languages' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1200, 9999 );
		register_nav_menus(
			array(
				'primary' => __( 'Primary Menu', 'ganjablog' ),
				'footer'  => __( 'Footer Menu', 'ganjablog' )
			)
		);
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);
		add_theme_support( 'responsive-embeds' );
		add_image_size( 'ganjablog-logo', 210, 40 );
		add_theme_support( 'custom-logo', array(
			'size' => 'ganjablog-logo'
		) );
	}
endif;
add_action( 'after_setup_theme', 'ganjablog_setup' );

// customize the template
function ganjablog_customize_register( $wp_customize ) {

	if ( ! isset( $wp_customize ) ) {
		return;
	}
	// image on home page
	$wp_customize->add_section( 'imageoner', array(
		"title"       => 'Homepage Images',
		"priority"    => 28,
		"description" => __( 'Upload images to display on the homepage.', 'ganjablog' )
	) );
	$wp_customize->add_setting( 'image_control_one', array(
		'default'    => '',
		'type'       => 'theme_mod',
		'capability' => 'edit_theme_options',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image_control_one', array(
			'label'    => __( 'Featured Home Page Image One', 'ganjablog' ),
			'section'  => 'imageoner',
			'settings' => 'image_control_one',
		) )
	);
	// parallax image
	$wp_customize->add_section( 'parallax', array(
		"title"       => 'Homepage Parallax Picture',
		"priority"    => 29,
		"description" => __( 'Upload image for parallax effect on the homepage.', 'ganjablog' )
	) );
	$wp_customize->add_setting( 'background_parallax', array(
		'default'    => '',
		'type'       => 'theme_mod',
		'capability' => 'edit_theme_options',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'background_parallax', array(
			'label'    => __( 'Parallax Image', 'ganjablog' ),
			'section'  => 'parallax',
			'settings' => 'background_parallax',
		) )
	);
}

add_action( 'customize_register', 'ganjablog_customize_register' );

// register sidebar
function ganjablog_widgets_init() {
	register_sidebar(
		array(
			'name'          => 'Specials',
			'id'            => 'promo',
			'description'   => 'Add promo and specials',
			'before_widget' => '<div id="%1$s" class="widget text-center %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);
}

add_action( 'widgets_init', 'ganjablog_widgets_init' );

// javascript detection
function ganjablog_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}

add_action( 'wp_head', 'ganjablog_javascript_detection', 0 );

// add threaded comments script
function ganjablog_scripts() {

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}

add_action( 'wp_enqueue_scripts', 'ganjablog_scripts' );

// Disable the emoji's
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}

add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 *
 * @return   array             Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

// Cut attributes from script and style tags
add_filter( 'style_loader_tag', 'codeless_remove_type_attr', 10, 2 );
add_filter( 'script_loader_tag', 'codeless_remove_type_attr', 10, 2 );
function codeless_remove_type_attr( $tag, $handle ) {
	return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
}

// clean up wordpress header
remove_action( 'wp_head', 'rsd_link' ); // remove really simple discovery link
remove_action( 'wp_head', 'wp_generator' ); // remove wordpress version
remove_action( 'wp_head', 'feed_links', 2 ); // remove rss feed links (make sure you add them in yourself if youre using feedblitz or an rss service)
remove_action( 'wp_head', 'feed_links_extra', 3 ); // removes all extra rss feed links
remove_action( 'wp_head', 'index_rel_link' ); // remove link to index page
remove_action( 'wp_head', 'wlwmanifest_link' ); // remove wlwmanifest.xml (needed to support windows live writer)
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // remove random post link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // remove parent post link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // remove the next and previous post links
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 ); // Remove shortlink

// Register Custom Navigation Walker
require_once 'class-wp-bootstrap-navwalker.php';

// add bootstrap 4 to navigation
function bootstrap_pagination( \WP_Query $wp_query = null, $echo = true ) {
	if ( null === $wp_query ) {
		global $wp_query;
	}
	$pages = paginate_links( [
			'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
			'format'       => '?paged=%#%',
			'current'      => max( 1, get_query_var( 'paged' ) ),
			'total'        => $wp_query->max_num_pages,
			'type'         => 'array',
			'show_all'     => false,
			'end_size'     => 3,
			'mid_size'     => 1,
			'prev_next'    => true,
			'prev_text'    => '« ',
			'next_text'    => ' »',
			'add_args'     => false,
			'add_fragment' => ''
		]
	);
	if ( is_array( $pages ) ) {
		//$paged = ( get_query_var( 'paged' ) == 0 ) ? 1 : get_query_var( 'paged' );
		$pagination = '<div class="pagination"><ul class="pagination m-auto">';
		foreach ( $pages as $page ) {
			$pagination .= '<li class="page-item"> ' . str_replace( 'page-numbers', 'page-link', $page ) . '</li>';
		}
		$pagination .= '</ul></div>';
		if ( $echo ) {
			echo $pagination;
		} else {
			return $pagination;
		}
	}

	return null;
}

/**
 * Get random posts from specific category
 *
 * @param int $category - ID of category to get random posts from
 * @param int $quantity - number of random posts
 *
 * @author Stas Pnomaryov
 */
function get_random_posts_from_category( $category = 1, $quantity = 4 ) {
	global $post;
	$args = array(
		'cat'            => $category,
		'posts_per_page' => $quantity,
		'orderby'        => 'rand',
		'post__not_in'   => array( $post->ID )
	);

	$query = new WP_Query( $args );
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class( 'col-md-6 col-lg-3' ); ?>>
                <div class="article-thumb">
					<?php the_post_thumbnail( 'medium', array( 'class' => 'img-fluid' ) ); ?>
                    <div class="article-summary">
						<?php the_title( sprintf( '<h3 class="card-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
                        <time class="article-date" datetime="<?php echo get_the_date( 'Y-m-d H:i' ) ?>">
							<?php echo get_the_date( 'd-m-Y', '', '' ); ?>
                        </time>
                        >
                        <span class="article-categories"><?php the_category( ', ' ); ?></span>
                    </div>
                </div>
            </article>
		<? }
	}
	wp_reset_postdata();
}

/**
 * Custom callback for outputting comments
 *
 * @return void
 * @author Keir Whitaker
 */
function bootstrap_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	?>
	<?php if ( $comment->comment_approved == '1' ): ?>
        <li class="comment">
        <div class="row">
            <div class="col-auto comment-author-avatar">
				<?php echo get_avatar( $comment ); ?>
            </div>
            <div class="col-auto">
                <div class="comment-author"><?php comment_author_link() ?></div>
                <div class="comment-time">
                    <time><a href="#comment-<?php comment_ID() ?>" pubdate><?php comment_date() ?>
                            в <?php comment_time() ?></a></time>
                </div>
            </div>
        </div>
        <div class="comment-text">
			<?php comment_text() ?>
        </div>
	<?php endif;
}

// remove dashicons
function wpdocs_dequeue_dashicon() {
	if ( current_user_can( 'update_core' ) ) {
		return;
	}
	wp_deregister_style( 'dashicons' );
}

add_action( 'wp_enqueue_scripts', 'wpdocs_dequeue_dashicon' );

// disable jquery for users
if ( ! is_admin() ) {
	add_action( "wp_enqueue_scripts", "my_jquery_enqueue", 11 );
}
function my_jquery_enqueue() {
	wp_deregister_script( 'jquery' );
}

// disable Guetenberg block styles
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );
function wps_deregister_styles() {
	wp_dequeue_style( 'wp-block-library' );
}