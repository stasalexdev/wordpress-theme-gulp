<?php get_header(); ?>
<section id="mainContent">
	<?php
	if ( function_exists( 'yoast_breadcrumb' ) ) {
		yoast_breadcrumb( '<div id="breadcrumbs">', '</div>' );
	}
	if ( have_posts() ) : ?>
        <h2 class="page-title"><?php printf( __( 'Search Results for: %s', 'ganjablog' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h2>
        <div class="row">
			<?php while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content', 'search' );
			endwhile; ?>
        </div>
		<?php
		echo bootstrap_pagination();
	else :
		get_template_part( 'template-parts/content', 'none' );
	endif; ?>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
