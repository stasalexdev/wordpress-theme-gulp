<?php if ( is_active_sidebar( 'promo' ) ) { ?>
	<?php $total_widgets = wp_get_sidebars_widgets();
	$sidebar_widgets = count($total_widgets['promo']); ?>
    <section id="mainWidgets">
        <div class="container-fluid">
            <div class="row has-widgets-<?php echo $sidebar_widgets ?>">
				<?php dynamic_sidebar( 'promo' ); ?>
            </div>
        </div>
    </section>
<?php } ?>