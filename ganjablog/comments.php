<div id="comments">
	<?php if ( post_password_required() ) : ?>
        <div class="alert alert-warning">
			<?php _e('This post is password protected', 'ganjablog'); ?>
        </div>
		<?php return;
	endif; ?>
	<?php if ( have_comments() ) : ?>
        <div class="comments-number"><?php comments_number(); ?></div>
        <ul class="comments-list">
			<?php wp_list_comments( array( 'callback' => 'bootstrap_comment' ) ); ?>
        </ul>
	<?php
    elseif ( ! comments_open() && ! is_page() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
        <div class="alert alert-warning">
			<?php _e('Comments are closed for this post', 'ganjablog'); ?>
        </div>
	<?php endif; ?>

	<?php
	ob_start();
	$commenter = wp_get_current_commenter();
	$req       = true;
	$aria_req  = ( $req ? " aria-required='true'" : '' );
	$comments_arg = array(
		'form'                => array(
			'class' => 'form-horizontal'
		),
		'fields'              => apply_filters( 'comment_form_default_fields', array(
			'autor' => '<div class="form-group">' . '<label for="author">' . esc_attr(__( 'Name', 'ganjablog' )) . '</label> ' . ( $req ? '<span>*</span>' : '' ) .
			           '<input id="author" name="author" class="form-control" type="text" value="" size="30"' . $aria_req . ' />' .
			           '<p id="d1" class="text-danger"></p>' . '</div>',
			'email' => '<div class="form-group">' . '<label for="email">E-mail</label> ' . ( $req ? '<span>*</span>' : '' ) .
			           '<input id="email" name="email" class="form-control" type="text" value="" size="30"' . $aria_req . ' />' .
			           '<p id="d2" class="text-danger"></p>' . '</div>',
			'url'   => ''
		) ),
		'comment_field'       => '<div class="form-group">'  . esc_attr(__( 'Comment', 'ganjablog' )) . '</label><span>*</span>' .
		                         '<textarea id="comment" class="form-control" name="comment" rows="3" aria-required="true"></textarea><p id="d3" class="text-danger"></p>' . '</div>',
		'class_submit'        => 'btn btn-primary',
		'label_submit' => esc_html(__( 'Send', 'ganjablog' )),
		// Change the title of the reply section
		'title_reply' => esc_html(__( 'Write a Reply or Comment', 'ganjablog' )),
		// Remove "Text or HTML to be displayed after the set of comment fields".
		'comment_notes_after' => '',
		'comment_notes_before' => '',
		'logged_in_as' => ''
	); ?>
	<?php comment_form( $comments_arg );
	echo str_replace( 'class="comment-form"', 'class="comment-form" name="commentForm" onsubmit="return validateForm();"', ob_get_clean() );
	?>
    <script>
        /* basic javascript form validation */
        function validateForm() {
            var form = document.forms["commentForm"];
            x = form["author"].value,
                y = form["email"].value,
                z = form["comment"].value,
                flag = true,
                d1 = document.getElementById("d1"),
                d2 = document.getElementById("d2"),
                d3 = document.getElementById("d3");

            if (x == null || x == "") {
                d1.innerHTML = "<?php echo __( 'Name is required', 'ganjablog' ); ?>";
                z = false;
            } else {
                d1.innerHTML = "";
            }

            if (y == null || y == "") {
                d2.innerHTML = "<?php echo __( 'Email is required', 'ganjablog' ); ?>";
                z = false;
            } else {
                d2.innerHTML = "";
            }

            if (z == null || z == "") {
                d3.innerHTML = "<?php echo __( 'Comment is required', 'ganjablog' ); ?>";
                z = false;
            } else {
                d3.innerHTML = "";
            }

            if (z == false) {
                return false;
            }
        }
    </script>
</div>