<?php get_header();
$theme       = get_bloginfo( 'template_directory' );
$name        = get_bloginfo( 'name' );
$description = get_bloginfo( 'description' ) ?>
<section id="aboutContent">
    <div class="jumbotron paral paralsec">
        <h2><?php _e( 'Welcome to ', 'ganjablog' );
			echo $name ?></h2>
        <p class="lead"><?php echo $description ?></p>
        <p class="lead">
            <a class="btn btn-primary btn-lg btn-md" href="/about" role="button"><?php _e( 'read more ', 'ganjablog' ); ?></a>
        </p>
    </div>
</section>
<section id="mainContent">
    <h2>Свежие материалы</h2>
	<?php if ( have_posts() ) : ?>
        <div class="row">
			<?php
			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content', get_post_format() );
			endwhile; ?>
        </div>
		<?php
		echo bootstrap_pagination();
	else :
		get_template_part( 'template-parts/content', 'none' );
	endif; ?>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
