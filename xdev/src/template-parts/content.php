<article id="post-<?php the_ID(); ?>" <?php post_class( 'col-md-4 col-lg-3' ); ?>>
    <div class="article-thumb">
		<?php the_post_thumbnail( 'medium', array( 'class' => 'img-fluid' ) ); ?>
        <div class="article-summary">
			<?php the_title( sprintf( '<h3 class="card-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
            <time class="article-date" datetime="<?php echo get_the_date( 'Y-m-d H:i' ) ?>">
				<?php echo get_the_date( 'd-m-Y', '', '' ); ?>
            </time> >
            <span class="article-categories"><?php the_category( ', ' ); ?></span>
        </div>
    </div>
</article>

