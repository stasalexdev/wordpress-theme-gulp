<section class="no-results not-found">
    <header class="entry-header">
        <h2><?php _e( 'Not found', 'ganjablog' ) ?></h2>
    </header>
    <div class="entry-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
            <p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'ganjablog' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>
		<?php elseif ( is_search() ) : ?>
            <p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'ganjablog' ); ?></p>
            <div class="row">
                <div class="col-lg-6">
					<?php get_search_form(); ?>
                </div>
            </div>
		<?php else : ?>
            <p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'ganjablog' ); ?></p>
            <div class="row">
                <div class="col-lg-6">
					<?php get_search_form(); ?>
                </div>
            </div>
		<?php endif; ?>
    </div>
</section>
