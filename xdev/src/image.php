<section id="mainContent">
	<?php while ( have_posts() ) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <nav id="image-navigation" class="navigation image-navigation">
                <div class="nav-links">
                    <div class="nav-previous"><?php previous_image_link( false, __( 'Previous Image', 'ganjablog' ) ); ?></div>
                    <div class="nav-next"><?php next_image_link( false, __( 'Next Image', 'ganjablog' ) ); ?></div>
                </div>
            </nav>
            <header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
            </header>
            <div class="entry-content">
                <div class="entry-attachment">
					<?php
					$image_size = apply_filters( 'ganjablog_attachment_size', 'large' );
					echo wp_get_attachment_image( get_the_ID(), $image_size );
					?>
                </div>
				<?php
				the_content();
				wp_link_pages(
					array(
						'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'ganjablog' ) . '</span>',
						'after'       => '</div>',
						'link_before' => '<span>',
						'link_after'  => '</span>',
						'pagelink'    => '<span class="sr-only">' . __( 'Page', 'ganjablog' ) . ' </span>%',
						'separator'   => '<span class="sr-only">, </span>',
					)
				);
				?>
            </div>
            <footer class="entry-footer">
				<?php
				// Retrieve attachment metadata.
				$metadata = wp_get_attachment_metadata();
				if ( $metadata ) {
					printf(
						'<span class="full-size-link"><span class="screen-reader-text">%1$s </span><a href="%2$s">%3$s &times; %4$s</a></span>',
						esc_html_x( 'Full size', 'Used before full size attachment link.', 'ganjablog' ),
						esc_url( wp_get_attachment_url() ),
						absint( $metadata['width'] ),
						absint( $metadata['height'] )
					);
				}
				?>
            </footer>
        </article>
		<?php
		if ( comments_open() || get_comments_number() ) {
			comments_template();
		}
		the_post_navigation(
			array(
				'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'ganjablog' ),
			)
		);
	endwhile; ?>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
