<!DOCTYPE html>
<?php $theme = get_bloginfo( 'template_directory' );
$name        = get_bloginfo( 'name' );
$description = get_bloginfo( 'description' );
$first = explode('.', trim($name)); ?>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="preload" as="style" href="<?php echo $theme ?>/css/styles.css">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
        <link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
	<?php endif; ?>
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $theme ?>/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $theme ?>/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $theme ?>/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $theme ?>/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="<?php echo $theme ?>/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>
    <style>
        #loading {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: #fff;
        }

        .box {
            position: fixed;
            top: 50%;
            left: 50%;
            perspective: 120px;
            transform: translateX(-50%) translateY(-50%);
        }

        .plane {
            width: 2em;
            height: 2em;
            background-color: #fc2f70;
            transform: rotate(0);
            animation: flip 1s infinite;
        }

        @keyframes flip {
            50% {
                transform: rotateY(180deg);
            }
            100% {
                transform: rotateY(180deg) rotateX(180deg);
            }
        }

        .paralsec {
            background-image: url(<?php if (get_theme_mod('background_parallax')) { echo get_theme_mod('background_parallax'); } else { echo "../images/parallax-3.jpg"; } ?>);
        }

    </style>
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400i,700,700i|Rubik:400,400i,700,700i&amp;subset=cyrillic"
          rel="stylesheet">
</head>
<body <?php body_class(); ?>>
<div id="loading">
    <div class="box">
        <div class="plane"></div>
    </div>
</div>
<div id="toTop"><span class="fas fa-angle-up"></span></div>
<div class="container-fluid p-0">
    <header id="mainHeader">
        <div class="navmenu navmenu-default navmenu-fixed-left offcanvas">
            <div class="close">&times;</div>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'primary',
				'depth'          => 2,
				'container'      => false,
				'menu_class'     => 'nav navmenu-nav flex-column',
				'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
				'walker'         => new WP_Bootstrap_Navwalker(),
			) );
			?>
        </div>
        <div id="primaryMenu" class="navbar navbar-expand-lg navbar-light">
            <div class="container-fluid">
                <a class="navbar-brand logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <h1 class="sr-only"><?php echo $first[0] ?></h1>
					<?php if (has_custom_logo()) { ?>
						<?php the_custom_logo(); ?>
					<?php } ?>
                </a>
                <button class="navbar-toggler"
                        type="button"
                        data-toggle="offcanvas"
                        data-target=".navmenu"
                        data-canvas="body">
                    <span class="fas fa-grip-lines"></span>
                </button>
                <nav class="collapse navbar-collapse" id="mainMenu" aria-label="Main Menu">
                    <span class="sr-only"><?php esc_html_e( 'Main Menu', 'ganjablog' ); ?></span>
					<?php
					wp_nav_menu( array(
						'theme_location' => 'primary',
						'depth'          => 2,
						'container'      => false,
						'menu_class'     => 'navbar-nav ml-auto',
						'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
						'walker'         => new WP_Bootstrap_Navwalker(),
					) );
					?>
                </nav>
            </div>
        </div>
    </header>
    <main id="mainContainer">
        <div class="container-fluid">
