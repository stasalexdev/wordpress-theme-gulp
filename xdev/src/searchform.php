<form role="search" method="get" class="input-group" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <input type="search" class="form-control" value="<?php echo get_search_query(); ?>" name="s"/>
    <div class="input-group-append">
        <button type="submit" class="btn btn-primary">искать</button>
    </div>
</form>