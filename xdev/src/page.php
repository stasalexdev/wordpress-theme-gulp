<?php get_header(); ?>
    <section id="mainContent">
		<?php while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/content', 'page' );

			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
		endwhile; ?>
    </section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>